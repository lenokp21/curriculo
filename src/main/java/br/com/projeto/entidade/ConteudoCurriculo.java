package br.com.projeto.entidade;

public class ConteudoCurriculo {
	
	private String item;
	private String subItem;
	private String conteudo;
	
	public ConteudoCurriculo() {
	}
	
	public ConteudoCurriculo(String item, String subItem, String conteudo) {
		this.item = item;
		this.subItem = subItem;
		this.conteudo = conteudo ;
	}

	public String getItem() {
		return item;
	}

	public void setItem(String item) {
		this.item = item;
	}

	public String getSubItem() {
		return subItem;
	}

	public void setSubItem(String subItem) {
		this.subItem = subItem;
	}

	public String getConteudo() {
		return conteudo;
	}

	public void setConteudo(String conteudo) {
		this.conteudo = conteudo;
	}
	
	

}
