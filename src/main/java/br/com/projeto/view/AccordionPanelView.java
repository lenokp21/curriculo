package br.com.projeto.view;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

import org.primefaces.event.TabChangeEvent;
import org.primefaces.event.TabCloseEvent;

import br.com.projeto.entidade.ConteudoCurriculo;

@ManagedBean
public class AccordionPanelView {

	private List<ConteudoCurriculo> conteudoCurriculos;

	@PostConstruct
	public void init() {
		conteudoCurriculos = new ArrayList<ConteudoCurriculo>();
		conteudoCurriculos.add(new ConteudoCurriculo("Dados Pessoais", "Dados", "Nome: Lennon da Cunha \n Idade: 28"));
		conteudoCurriculos.add(new ConteudoCurriculo("ExperiÍncias Proficionais", "TRF", "Programando em PL/SQL"));

	}

	public List<ConteudoCurriculo> getConteudoCurriculos() {
		return conteudoCurriculos;
	}

	public void setConteudoCurriculos(List<ConteudoCurriculo> conteudoCurriculos) {
		this.conteudoCurriculos = conteudoCurriculos;
	}

	public void onTabChange(TabChangeEvent event) {
		FacesMessage msg = new FacesMessage("Tab Changed", "Active Tab: " + event.getTab().getTitle());
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}

	public void onTabClose(TabCloseEvent event) {
		FacesMessage msg = new FacesMessage("Tab Closed", "Closed tab: " + event.getTab().getTitle());
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}

}
