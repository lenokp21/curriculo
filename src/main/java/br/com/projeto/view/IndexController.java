package br.com.projeto.view;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

import org.apache.commons.mail.EmailException;

import br.com.projeto.entidade.Mensagem;
import br.com.projeto.util.EmailUtil;

@ManagedBean
public class IndexController {
	
	private Mensagem mensagem ;
	
	@PostConstruct
	public void init(){
		mensagem = new Mensagem();
		mensagem.setDestino("lennonkp21@gmail.com");/*
		mensagem.setMensagem("Teste de envio de E-mail!");
		mensagem.setTitulo("Titulo da Mensagem");*/
	}
	
	public Mensagem getMensagem() {
		return mensagem;
	}
	
	public void setMensagem(Mensagem mensagem) {
		this.mensagem = mensagem;
	}
	
	public void enviaEmail() {
		try {
			EmailUtil.enviaEmail(mensagem);
		} catch (EmailException ex) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro! Occoreu um erro ao enviar a mensagem.", "Erro"));
			Logger.getLogger(IndexController.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
	
	public void limpaCampos() {
		mensagem = new Mensagem();
	}

}
