package br.com.projeto.teste;

import org.apache.commons.mail.EmailException;

import br.com.projeto.entidade.Mensagem;
import br.com.projeto.util.EmailUtil;

public class Teste {

	public static void main(String[] args) {
		
		Mensagem mensagem = new Mensagem();
		mensagem.setDestino("lennonkp21@gmail.com");
		mensagem.setMensagem("Teste de envio de E-mail!");
		mensagem.setTitulo("Titulo da Mensagem");
		
		try {
			EmailUtil.conectaEmail();
			EmailUtil.enviaEmail(mensagem);
		} catch (EmailException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
